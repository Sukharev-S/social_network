import preloader from "../../../assets/preloader.svg";
import React from "react";

let Preloader = (props) => {
	return (
		<div style={{position: 'absolute'}}>
			<img src={preloader}/>
		</div>
	)
}

export default Preloader;