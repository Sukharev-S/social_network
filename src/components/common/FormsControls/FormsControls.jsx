import React from 'react';
import s from './FormsControls.module.css';

// export const Textarea = ({input, meta, ...props}) => {
//     const hasError = meta.touched && meta.error;
//     return (
//         <div className={s.formControl + ' ' + (hasError ? s.error : '')}>
//             <div>
//                 <textarea {...input} {...props}/>
//             </div>
//             {hasError && <span>meta.error</span>}
//         </div>
//     )
// }

const Element = Element => ({ input, meta: {touched, error}, ...props }) => {
    const hasError = touched && error;
    return (
        <div className={ s.formControl + " " + (hasError ? s.error : "") }>
            <Element {...input} {...props} />
            { hasError && <span> { error } </span> }
        </div>
    );
};

export const Textarea = Element("textarea");
export const Input = Element("input");