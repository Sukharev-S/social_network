import React, {useState} from 'react';
import s from "./Pagination.module.css";

let Pagination = ({totalItemsCount, pageSize, currentPage, onPageChanged, partionSize = 10}) => {
    let pageCount = Math.ceil(totalItemsCount / pageSize);
    let pages = [];
    for (let i = 1; i <= pageCount; i++) {
        pages.push(i);
    }

    let partionCount = Math.ceil(pageCount / partionSize);
    let [partionNumber, setPartionNumber] = useState(1);
    let leftPartionPageNumber = (partionNumber - 1) * partionSize + 1;
    let rightPartionPageNumber = partionNumber * partionSize;

    return <div className={s.listPageNumber}>
        {
            partionNumber > 1 && <button onClick={() => {setPartionNumber(partionNumber - 1)}}
                                         className={s.btnPrev}>PREV</button>
        }
        {
            pages
                .filter(p => p >= leftPartionPageNumber && p <= rightPartionPageNumber)
                .map(p => <span className={s.pageNumber + ' ' + (currentPage === p && s.selectedPage)}
                                key={p}
                                onClick={() => onPageChanged(p)}>{p}</span>)
        }
        {
            partionCount > partionNumber && <button onClick={() => {setPartionNumber(partionNumber + 1)}}
                                                    className={s.btnNext}>NEXT</button>
        }
    </div>
}
export default Pagination;