import s from './Message.module.css';
import React from 'react';

const Message = (props) => {
    return (
        <div className={s.wrapper}>
            <div>{props.message}</div>
        </div>
    )
};

export default Message;