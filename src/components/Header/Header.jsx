import React from 'react';
import s from './Header.module.css'
import logo from '../Header/logo.png'
import {NavLink} from "react-router-dom";

const Header = (props) => {
    return (
        <div className={s.wrapper}>
            <img className={s.logo} src={logo}/>
            <header className={s.title}>My Social NetWork</header>
          <div className={s.loginBlock}>
              {props.isAuth
                  ? <div>{props.login}<button onClick={props.logout}>Log out</button></div>
                  : <NavLink className={s.logoLink} to={'/login'}>Login</NavLink>}

          </div>
        </div>
    )
};

export default Header;