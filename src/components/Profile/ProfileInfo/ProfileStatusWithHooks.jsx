import React, {useEffect, useState} from 'react';
import s from './ProfileStatus.module.css';

const ProfileStatusWithHooks = (props) => {

    let [editMode, setEditMode] = useState(false);
    let [status, setStatus] = useState(props.status);

    useEffect(() => {setStatus(props.status)}, [props.status]);

    const activateEditMode = () => {
        setEditMode(true);
    }
    const deactivateEditMode = () => {
        setEditMode(false);
        props.updateStatus(status);
    }
    const onStatusChange = (e) => {
        setStatus(e.currentTarget.value)
    }

    return (
        <div>
            {!editMode &&
            <div>
                <span onClick={activateEditMode}>{props.status || '**********'}</span>
            </div>
            }
            {editMode &&
            <div>
                <input onChange={onStatusChange}
                       autoFocus
                       onBlur={deactivateEditMode}
                       value={status}></input>
            </div>
            }
        </div>
    )
    // state = {
    //     editMode: false,
    //     status: this.props.status
    // }
    // activateEditMode = () => {
    //     this.setState({
    //         editMode: true
    //     });
    // }
    // deactivateEditMode = () => {
    //     this.setState({editMode: false	});
    //     this.props.updateStatus(this.state.status);
    // }
    // onStatusChange = (e) => {
    //     this.setState({status: e.currentTarget.value})
    // }
    // componentDidUpdate(prevProps, prevState, snapshot) {
    //     if(prevProps.status != this.props.status) {
    //         this.setState({
    //             status: this.props.status
    //         })
    //     }
    // }
    //
    // render() {
    //     return (
    //         <div>
    //             {!this.state.editMode &&
    //             <div>
    //                 <span onClick={this.activateEditMode}>{this.props.status || '**********'}</span>
    //             </div>
    //             }
    //             {this.state.editMode &&
    //             <div>
    //                 <input onChange={this.onStatusChange} autoFocus onBlur={this.deactivateEditMode} value={this.state.status}></input>
    //             </div>
    //             }
    //         </div>
    //     )
    // }
}

export default ProfileStatusWithHooks;