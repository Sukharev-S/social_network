import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import {follow,	setCurrentPage,	unfollow,
	toggleFollowingProgress, requestUsers} from '../../Redux/users-reducer';
import Users from './Users';
import Preloader from "../common/Preloader/Prelader";
import {withAuthRedirect} from "../../hoc/withAuthRedirect";
import {compose} from "redux";
import {
	getUsers,
	getCurrentPage,
	getFollowingInProgress,
	getIsFetching,
	getPageSize,
	getTotalUsersCount
} from "../../Redux/users-selectors";


class UsersContainer extends PureComponent {
	componentDidMount() {
		let {currentPage, pageSize} = this.props;
		this.props.requestUsers(currentPage, pageSize);
	}

	onPageChanged = pageNumber => {
		let {setCurrentPage, requestUsers, pageSize} = this.props;
		setCurrentPage(pageNumber);
		requestUsers(pageNumber, pageSize);
	}

	render() {
		return <>
			{this.props.isFetching ? <Preloader/> : null}
			<Users totalUsersCount={this.props.totalUsersCount}
			       pageSize={this.props.pageSize}
			       currentPage={this.props.currentPage}
			       onPageChanged={this.onPageChanged}
			       users={this.props.users}
			       follow={this.props.follow}
			       unfollow={this.props.unfollow}
			       followingInProgress={this.props.followingInProgress}
			/>
		</>
	}

} //---------------  End UsersAPIComponent

let mapStateToProps = (state) => {
	return {
		users: getUsers(state),
		pageSize: getPageSize(state),
		totalUsersCount: getTotalUsersCount(state),
		currentPage: getCurrentPage(state),
		isFetching: getIsFetching(state),
		followingInProgress: getFollowingInProgress(state)
	}
};

export default compose(
	//withAuthRedirect,
	connect(mapStateToProps, {follow, unfollow,	setCurrentPage, toggleFollowingProgress, requestUsers})
)(UsersContainer);