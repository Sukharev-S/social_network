import {profileReducer} from './profile-reducer';
import {dialogsReducer} from './dialogs-reducer';

let store = {
    _state: {
        dialogsPage: {
            messages: [
                {id: 1, message: 'Hi'},
                {id: 2, message: 'How are you?'},
                {id: 3, message: 'How is your cat?'},
                {id: 4, message: 'Yo'},
                {id: 5, message: ':))'}
            ],
            dialogs: [
                {id: 1, name: 'Sergey'},
                {id: 2, name: 'Katy'},
                {id: 3, name: 'Alex'},
                {id: 4, name: 'Natasha'},
                {id: 5, name: 'Pety'}
            ],
            newMessageText: "Yo"
        },
        profilePage: {
            posts: [
                {id: 1, message: 'Hi, how are you?', likesCount: 12},
                {id: 2, message: 'It\'s my first post', likesCount: 11}
            ],
            newPostText: "It's fun!"
        }
    },
    _callSubscriber() {
    },
    getState() {
        return this._state;
    },
    subscribe(observer) {
        this._callSubscriber = observer;
    },
    dispatch(action) {
        this._state.profilePage = profileReducer(this._state.profilePage, action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage, action);
        this._callSubscriber();
    }
};

export default store;